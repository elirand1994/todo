import log from "@ajar/marker";
import fs from 'fs/promises';
import {generateId,path} from './etc.js';
import STATES from './types';
import Task from './Task'
class Controller {
    private todos;
    constructor(todos: Task[]){
        this.todos = todos;
    }
    createTask = async (args : string) => {
        log.blue("Creating the task...")
        const [title,description] = args;
        const task : Task = {
            "title" : title,
            'description' : description,
            "isComplete" : false,
            "id" : generateId()
        };
        this.todos.push(task);
        await this.saveToDos(this.todos);
    }
    
    readTasks  = async (params : any) =>{
        let list =[];
        const [read] = params;
        if (read !== undefined) {
            switch(read){
                case STATES.COMPLETED:
                    list = this.todos.filter((task: { isComplete: boolean; })=> task.isComplete === true);
                    break;
                case STATES.ACTIVE:
                    list = this.todos.filter(((task: { isComplete: boolean; }) => task.isComplete === false));
                    break;
                case STATES.ALL:
                    list = this.todos;
                    break;
                default:
                    list = this.todos;
                    break;
            }
        }
        else {
            list = this.todos;
        }
        log.magenta('LIST OF TASKS:')
        for (const task of list){
            log.cyan(`Task ID : ${task.id}`);
            log.magenta(`Title : ${task.title}`)
            if (task.description !== undefined) {
                log.magenta(`Description : ${task.description}`)
            }
            task.isComplete? log.green('Task has been completed!') : log.red('Task has not been commpleted yet...');
            log.yellow('------------------------------------');
        }
    }
    
    removeCompleted = async () => {
        const list = this.todos.filter((task: { isComplete: boolean; }) => task.isComplete === false);
        this.saveToDos(list);
    }
    
    updateTask = async (params : string) =>{
        const [id] = params;
        const task = this.todos.find((element: { id: any; }) => element.id === id);
        if (task === undefined) {
            throw new Error(`Task ${id} is not found!`);
        }
        task.isComplete = !task.isComplete;
        console.log(task)
        this.saveToDos(this.todos);
    }
    
    deleteTask = async (id : any ) =>{
        const [taskId] = id;
        const updatedTodos = this.todos.filter((element: { id: any; }) => element.id !== taskId);
        this.saveToDos(updatedTodos);
    }
    
    showInfo = () =>{
        const text = `
        How to use:
         npm run build -> builds the project
         npm run start create "title" -> creates a new task with the given title
         npm run start read -> retrieves all the tasks and displays in a formated version
         npm run start update "task_id" "true|false" -> updates a given task based on the id, completed or not
         npm run start delete "task_id" -> deletes the given task from the todolist
         npm run start clear -> removes all the completed tasks`;
        log.cyan(text);
    }
    
    runTask = async (args : any) =>{
        try {
        const task = args[0];
        let params;
        switch (task){
            case STATES.CREATE:
                params = args.slice(1);
                await this.createTask(params);
                break;
            case STATES.READ:
                params = args.slice(1);
                await this.readTasks(params);
                break;
            case STATES.UPDATE:
                params = args.slice(1);
                await this.updateTask(params);
                break;
            case STATES.DELETE:
                params = args.slice(1);
                await this.deleteTask(params);
                break;
            case STATES.DELETE_COMPLETED:
                await this.removeCompleted();
                break;
            default:
                throw new Error(`No such command : ${task} found!`);
    
            }
        } catch(error){
        log.red(error)
        }
    }

    saveToDos = async (todos : Task[]) =>{
        await fs.writeFile(path,JSON.stringify(todos))
        
    }
    
}

export default Controller;