const enum STATES {
    CREATE = "create",
    READ = "read",
    UPDATE = "update",
    DELETE = "delete",
    COMPLETED = "completed",
    ALL = "all",
    ACTIVE = "active",
    DELETE_COMPLETED = "clear"
}

export default STATES;