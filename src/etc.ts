export const path = 'src/todos.json';
import fs from 'fs/promises';

export const generateId = () => {
    return Math.random().toString(32).slice(2);
}

export const checkIfTodosExist = async () =>{
    try {
        await fs.access(path);
        return true;
    } catch(err){
        return false;
    }
}
export const load = async () =>{
    const flag = await checkIfTodosExist();
    if (!flag){
        console.log('Creating...')
        await fs.writeFile(path,JSON.stringify([]),'utf-8');
    }
    const json = await fs.readFile(path,'utf-8');
    const todos = await JSON.parse(json);
    return todos;
}
