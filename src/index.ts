import Controller from './controller.js';
import {load} from './etc.js';
const main = async () =>{
    const args = process.argv.slice(2);
    const todos = await load();
    const controller = new Controller(todos)
    await controller.runTask(args);
}


main();